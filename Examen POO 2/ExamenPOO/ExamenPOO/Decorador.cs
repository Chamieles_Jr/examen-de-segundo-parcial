﻿using System;
using System.Collections.Generic;
using System.Text;
//Implementación del patrón decorator
namespace ExamenPOO
{
    //Clase abstracta que hereda de InformacionHotDog
    public abstract class Decorador:InformacionHotDog
    {
        //Se define como protected porque sera utilizada o empleada por los hijos de la clase Decorador
        protected InformacionHotDog _HotDog_;
        //hotDog es la agregacion que conecta con InformacionHotDog
        public Decorador (InformacionHotDog hotDog)
        {
            _HotDog_ = hotDog;
        }
    }
}

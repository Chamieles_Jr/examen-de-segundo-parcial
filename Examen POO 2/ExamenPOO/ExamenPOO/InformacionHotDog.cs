﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
namespace ExamenPOO
{
    //Clase de Jerarquía mayor
    public abstract class InformacionHotDog
    {
        //Operaciones Abstrractas
        public abstract double precio { get;}
        public abstract string DescripcionHotDog { get;}
    }
}

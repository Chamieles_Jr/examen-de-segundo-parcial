﻿using ExamenPOO.Ingredientes;
using System;
using System.Linq;
using System.Threading.Tasks;
namespace ExamenPOO
//Nombre: Chavez Mieles Anthony Xavier
//Fecha: 11/03/21
//Curso: Tercer Nivel "c"
{
    class Program
    {
        static void Main(string[] args)
        {
            //Se crea un objeto llamado PerroCaliente el cual se puede modificar al gusto con cualquier ingrediente
            InformacionHotDog PerroCaliente = new HotDog_Basico();
            //Se agregan los ingredientes a la instancia creada "PerroCaliente"
            PerroCaliente = new Mayonesa (PerroCaliente);
            PerroCaliente = new Papas(PerroCaliente);
            PerroCaliente = new Picante(PerroCaliente);
            PerroCaliente = new SalsaDeTomate(PerroCaliente);
            PerroCaliente = new Mostaza(PerroCaliente);

            //Se crea un objeto llamado PerroCaliente1 el cual se puede modificar al gusto con cualquier ingrediente
            InformacionHotDog PerroCaliente1 = new HotDog_Completo();
            //Se agregan los ingredientes a la instancia creada "PerroCaliente1"
            PerroCaliente1 = new Picante(PerroCaliente1);
            PerroCaliente1 = new Mayonesa(PerroCaliente1);
            PerroCaliente1 = new Mostaza(PerroCaliente1);
            PerroCaliente1 = new SalsaDeTomate(PerroCaliente1);
            PerroCaliente1 = new Papas(PerroCaliente1);

            //Se crea un objeto llamado PerroCalient2 el cual se puede modificar al gusto con cualquier ingrediente
            InformacionHotDog PerroCaliente2 = new HotDog_Especial();
            //Se agregan los ingredientes a la instancia creada "PerroCaliente2"
            PerroCaliente2 = new Papas(PerroCaliente2);
            PerroCaliente2 = new Picante(PerroCaliente2);
            PerroCaliente2 = new Mostaza(PerroCaliente2);
            PerroCaliente2 = new SalsaDeTomate(PerroCaliente2);
            PerroCaliente2 = new Mayonesa(PerroCaliente2);

            //Se muestra en pantalla la informacion de la compra y la factura
            Console.WriteLine("                                     FACTURA DE LA COMPRA                                     ");
            Console.WriteLine("NOMBRE: Anthony Xavier Chávez Mieles");
            Console.WriteLine("FECHA: 11/03/2021");
            Console.WriteLine("DIRECCION:  Manta");
            Console.WriteLine("CEDULA: 1215546564");
            Console.WriteLine("");
            Console.WriteLine($"PEDIDO: {PerroCaliente.DescripcionHotDog}");
            Console.WriteLine($"tiene un costo final de: ${ PerroCaliente.precio}");
            Console.WriteLine("");
            Console.WriteLine($"PEDIDO: {PerroCaliente1.DescripcionHotDog}");
            Console.WriteLine($"tiene un costo final de: ${ PerroCaliente1.precio}");
            Console.WriteLine("");
            Console.WriteLine($"PEDIDO: {PerroCaliente2.DescripcionHotDog}");
            Console.WriteLine($"tiene un costo final de: ${ PerroCaliente2.precio}");
            Console.ReadKey();
            //Observación:
            //Este patron de diseño se puede aplicar si queremos hacer un sistema de facturacion de cualquier empresa ya que nos permite agregar muchas caracteristicas al objeto
        }
    }
}

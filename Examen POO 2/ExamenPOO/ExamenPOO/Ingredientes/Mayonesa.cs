﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenPOO.Ingredientes
{
    //Clase Mayone que hereda Decorador
    public class Mayonesa : Decorador
    {
        public Mayonesa (InformacionHotDog hotDog) : base(hotDog) { }//Se define el ingrediente al que se le agregará al HotDog
        public override double precio => _HotDog_.precio + 0.50;//Se le agrega el costo individual del ingrediente al total del HotDog
        public override string DescripcionHotDog => string.Format($"{_HotDog_.DescripcionHotDog} Mayonesa -");//Se agrega la descripción

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamenPOO.Ingredientes
{
    //Clase SalsaDeTomate que hereda Decorador
    public class SalsaDeTomate: Decorador
    {
        public SalsaDeTomate(InformacionHotDog hotDog) : base(hotDog) { } //Se define el ingrediente al que se le agregará al HotDog
        public override double precio => _HotDog_.precio + 0.50;//Se le agrega el costo individual del ingrediente al total del HotDog
        public override string DescripcionHotDog => string.Format($"{_HotDog_.DescripcionHotDog} Salsa de Tomate -");//Se agrega la descripción
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace ExamenPOO.Ingredientes
{
    //Clase Mostaza que hereda Decorador
    public class Mostaza : Decorador
    {
        public Mostaza(InformacionHotDog hotDog) : base(hotDog) { }//Se define el ingrediente al que se le agregará al HotDog
        public override double precio => _HotDog_.precio + 0.50;//Se le agrega el costo individual del ingrediente al total del HotDog
        public override string DescripcionHotDog => string.Format($"{_HotDog_.DescripcionHotDog} Mostaza -");//Se agrega la descripción
    }
}

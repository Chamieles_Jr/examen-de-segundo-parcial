﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenPOO
{
    //Clase heredada de InformacionHotDog
    public class HotDog_Basico:InformacionHotDog
    {
        public override double precio => 1.00;//Parámetro del costo
        public override string DescripcionHotDog => "HotDog Basico con: ";//Descripcion del HotDog
    }
}

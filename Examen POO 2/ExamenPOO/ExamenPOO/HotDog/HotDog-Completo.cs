﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenPOO
{
    //Clase heredada de InformacionHotDog
    public class HotDog_Completo: InformacionHotDog
    {
        public override double precio => 2.50;//Parámetro del costo
        public override string DescripcionHotDog => "HotDog Completo con: ";//Descripcion del HotDog
    }
}
